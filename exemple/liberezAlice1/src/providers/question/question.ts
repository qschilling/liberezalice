import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable }	from 'rxjs/Observable';
import { Question }	from '../../models/Question';

@Injectable()
export class QuestionProvider {
  public lesQuestions:Array<Question>;  

  constructor(public http:HttpClient) {
    this.lesQuestions = new Array<Question>();
  }

  getLesQuestions(){
    var promise = new Promise((resolve, reject) => {
      let	data:Observable<any>;
      data = this.http.get('../../assets/questions.json');
      data.subscribe(result	=>	{
        for (let uneQuestion of result) {
          this.lesQuestions.push(new Question(uneQuestion));
          }
        resolve(this.lesQuestions);
        });
      })
      return promise;
    }

  getLaQuestion(unNumQuestion:number):Question{
    return this.lesQuestions.find(x => x.numero == unNumQuestion);
    }
  }
