export	class	Question	{
    public numero:number;
    public intitule:string;
    public typeQuestion:string;
    public video:string;
    public images:Array<string>;
    public reponses:Array<string>;
    public laReponse:string;
    public detail:string;

    constructor(uneQuestion:any) {
        this.numero = uneQuestion.numero;
        this.intitule = uneQuestion.question;
        this.typeQuestion = uneQuestion.typeQuestion;
        this.video = uneQuestion.video;
        this.images = uneQuestion.images;
        this.reponses = uneQuestion.reponses;
        this.laReponse = uneQuestion.laReponse;
        this.detail = uneQuestion.detail;
        }
    }