import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { QuestionProvider } from '../../providers/question/question';
import { Question } from '../../models/Question';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public laQuestion:Question;
  public numQuestionEnCours:number;

  constructor(public unQuesProvider:QuestionProvider, public navCtrl: NavController) {
    this.numQuestionEnCours = 1;
    unQuesProvider.getLesQuestions().then(data => {
      this.laQuestion = unQuesProvider.getLaQuestion(this.numQuestionEnCours);
      this.setViewIntitule(this.laQuestion);
      this.setLesReponsesView();
    });
    }


  questionSuivante(){
    if (this.numQuestionEnCours < 16 ) {
      this.numQuestionEnCours++;
      this.laQuestion = this.unQuesProvider.getLaQuestion(this.numQuestionEnCours);
      this.setViewIntitule(this.laQuestion);
      this.setLesReponsesView();

      }
    }

  questionPrecedente(){
    if (this.numQuestionEnCours > 1 ) {
      this.numQuestionEnCours--;
      this.laQuestion = this.unQuesProvider.getLaQuestion(this.numQuestionEnCours);
      this.setViewIntitule(this.laQuestion);
      this.setLesReponsesView();
      }
    }

  setViewIntitule(uneQuestion:Question){
    document.getElementById("intiluleQuestion").innerHTML = uneQuestion.intitule;
    }

  setLesReponsesView(){
    console.log("coucou" + this.laQuestion.typeQuestion);

    if (this.laQuestion.typeQuestion === "normal")
    console.log();
    }  
  }
