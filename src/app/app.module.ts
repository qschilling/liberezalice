import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { PopoverComponent } from '../components/popover/popover';
import { QuestionProvider } from '../providers/question/question';
import { ReponseProvider } from '../providers/reponse/reponse';
import { VideoProvider } from '../providers/video/video';
import { QuestionPage } from '../pages/question/question';
import { ReponsePage } from '../pages/reponse/reponse';
import { FinalPage } from '../pages/final/final';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    QuestionPage,
    ReponsePage,
    FinalPage,
    PopoverComponent
    
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    QuestionPage,
    ReponsePage,
    FinalPage,
    PopoverComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    QuestionProvider,
    ReponseProvider,
    VideoProvider
  ]
})
export class AppModule {}
