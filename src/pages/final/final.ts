import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ReponsePage } from '../reponse/reponse';
import { ReponseProvider } from '../../providers/reponse/reponse';
import { VideoProvider } from '../../providers/video/video';
import { Video } from '../models/Video';

/**
 * Generated class for the FinalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-final',
  templateUrl: 'final.html',
})
export class FinalPage {

  reponse:ReponsePage;
  cota:string;
  nbBonneReponse:number;
  videoFinale:Video;
  video:string;
  bilan:string;

  constructor(public videoProvider:VideoProvider, public reponseProvider:ReponseProvider, public navCtrl: NavController, public navParams: NavParams) {
      reponseProvider.loadDataJSONfileReponse().then(result => {
      this.nbBonneReponse = reponseProvider.getBonneReponse();
      this.cota=((this.nbBonneReponse / 17) * 100).toFixed(0);
      this.getVideoFinale();
    });  
  }

  getVideoFinale()
  {
    this.videoProvider.loadDataJSONfileVideo().then(
      result => {
        if(this.nbBonneReponse>8)
        {
          this.videoFinale = this.videoProvider.getVideo("9");
          this.video = this.videoFinale.video;
          this.bilan="Bravo, je suis libre grâce à toi !";
        }
        else
        {
          this.videoFinale = this.videoProvider.getVideo("8");
          this.video = this.videoFinale.video;
          this.bilan="Dommage, je resterai encore enfermée ici ..."
        }
        
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FinalPage');
  }

}
