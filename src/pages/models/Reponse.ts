export class Reponse {
    public id:string;
    public reponse:string;
    public detail:string;

    constructor(uneReponse:any)
    {
        this.id = uneReponse.id;
        this.reponse = uneReponse.reponse;
        this.detail = uneReponse.detail;
    }

    
}