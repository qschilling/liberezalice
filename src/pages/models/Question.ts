export class Question {
    public id:string;
    public question:string;
    public typeQuestion:string;
    public typeImage:string;
    public images:string[];
    public reponses:string[];
    public isValide:boolean;    

    constructor (uneQuestion:any)
    {
        this.id = uneQuestion.id;
        this.question = uneQuestion.question;
        this.typeQuestion = uneQuestion.typeQuestion;
        this.typeImage = uneQuestion.typeImage;
        this.images = uneQuestion.images;
        this.reponses = uneQuestion.reponses;
        this.isValide = uneQuestion.isValide;
    }
}