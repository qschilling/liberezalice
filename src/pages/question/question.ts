import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpClient} from '@angular/common/http'; 
import { Observable } from 'rxjs/Observable';
import { Question } from '../../pages/models/Question';
import { QuestionProvider } from '../../providers/question/question';
import { ReponseProvider } from '../../providers/reponse/reponse';
import { ReponsePage } from '../reponse/reponse';
import { Reponse } from '../models/Reponse';

@IonicPage()
@Component({
  selector: 'page-question',
  templateUrl: 'question.html',
})

export class QuestionPage {

  public laQuestion:Question;
  public numQuestionEnCours:number;
  question:any;
  reponse:Reponse;
  reponseUtilisateur:string;
  images:any[];

  constructor(public reponseProvider:ReponseProvider, public questionProvider:QuestionProvider, public httpClient: HttpClient, public navCtrl: NavController, public navParams: NavParams) {
    this.numQuestionEnCours = 1;
    questionProvider.getQuestions().then(data => {
      this.laQuestion = this.navParams.get("uneQuestion");
      this.reponseUtilisateur = this.navParams.get("reponseUtilisateur");
      this.afficherQuestion();
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuestionPage');
  }

  afficherQuestion(){
    this.laQuestion=this.questionProvider.getQuestion(this.numQuestionEnCours.toString());
    this.question = this.laQuestion.question;
    this.images = this.laQuestion.images;
  }

  getQuestionSuivante(){
    this.numQuestionEnCours++;
    this.afficherQuestion();
  }

  getQuestionPrecedente(){
    
    if(this.numQuestionEnCours==1)
    {
      this.navCtrl.pop();
    }
    else
    {
      this.numQuestionEnCours--; 
      this.afficherQuestion();
    }
  }

  getConfirmedReponse(){
    let uneQuestion = this.laQuestion;
    let reponseUtilisateur = "hello";
    this.getQuestionSuivante();
    this.questionProvider.validerQuestion(uneQuestion.id);
    this.navCtrl.push(ReponsePage,{this:uneQuestion.id, reponseUtilisateur});
  }

}
