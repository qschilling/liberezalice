import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, NavPush } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { ReponseProvider } from '../../providers/reponse/reponse';
import { QuestionProvider } from '../../providers/question/question';
import { Reponse } from '../models/Reponse';
import { FinalPage } from '../final/final';
import { Question } from '../models/Question';


@IonicPage()
@Component({
  selector: 'page-reponse',
  templateUrl: 'reponse.html',
})
export class ReponsePage {
  numReponse:number;
  detail:any;
  reponse:Reponse;
  bonneReponse:any;
  bonneOuMauvaise:any;
  nbBonneReponse:number;
  final:FinalPage;
  public laQuestion:Question;
  public reponseUtilisateur:string;

  constructor(public questionProvider:QuestionProvider, public reponseProvider:ReponseProvider, public httpClient:HttpClient, public navCtrl: NavController, public navParams: NavParams) {
    this.detail="";
    this.bonneOuMauvaise="Oui !";
    reponseProvider.loadDataJSONfileReponse().then(result => {
      this.numReponse ++;
      if(this.numReponse <= 17)
      {
        this.numReponse++;
        this.afficherDetails();
      }
    });
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReponsePage');
  }

  afficherDetails() {
    this.reponse = this.reponseProvider.getReponse(this.numReponse.toString());
    console.log(this.reponse.id);
    this.detail = this.reponse.detail;
  }

  continuer()
  {
    this.reponseProvider.loadDataJSONfileReponse().then(result => {
      this.numReponse ++;
      if(this.numReponse <= 17)
      {
        this.reponse = this.reponseProvider.getReponse(this.numReponse.toString());
        this.afficherDetails();
        this.navCtrl.pop(); 
      }
      else
      {
        this.navCtrl.push(FinalPage);
      }
    });
  }

  verifierReponse(reponseUtilisateur:any)
  {
    this.reponseProvider.loadDataJSONfileReponse().then(result => {
      this.reponse = this.reponseProvider.getReponse(this.numReponse.toString());
    this.bonneReponse = this.reponse.reponse;
    if(reponseUtilisateur==this.bonneReponse)
    {
      this.bonneOuMauvaise="Bonne réponse ! Bravo !"
      this.reponseProvider.ajouterBonneReponse();
    }
    else{
      this.bonneOuMauvaise="Oh non ! Mauvaise réponse !"
    }
    });
    
  }

  public getBonneReponse()
  {
    return this.nbBonneReponse;
  }
}
