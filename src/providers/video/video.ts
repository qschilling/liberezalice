import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Video } from '../../pages/models/Video';

/*
  Generated class for the VideoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class VideoProvider {

  videos:Array<Video>;

  constructor(public http: HttpClient) {
    this.videos=new Array<Video>();
    console.log('Hello VideoProvider Provider');
  }

  getVideo(num:string):Video
  {
    return this.videos.find(x => x.id == num);
  }

  loadDataJSONfileVideo()
  {
    var promise = new Promise(resolve=>{
      let data:Observable<any>;
      data=this.http.get('../../assets/video.json');
      data.subscribe(result => { 
          for(let uneVideo of result) {
            this.videos.push(new Video(uneVideo));
          }
          resolve(this.videos);
          });
        });
        return promise;
  }
}
