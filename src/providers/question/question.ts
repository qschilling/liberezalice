import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Question }	from '../../pages/models/Question';
import { Observable }	from 'rxjs/Observable';
import { NavController } from 'ionic-angular';


@Injectable()
export class QuestionProvider {
  public lesQuestions:Array<Question>;
  public laQuestion:Question;
  public numQuestionEnCours:number;
  question:any; 

  constructor(public http: HttpClient) {
    this.lesQuestions = new Array<Question>();
  }

  getQuestions()
  {
    var promise = new Promise((resolve, reject) => {
      let	data:Observable<any>;
      data = this.http.get('../../assets/question.json');
      data.subscribe(result	=>	{
        for (let uneQuestion of result) {
          this.lesQuestions.push(new Question(uneQuestion));
          }
        resolve(this.lesQuestions);
        });
      })
      return promise;
  }

  getQuestion(unNumQuestion:string):Question{
    return this.lesQuestions.find(x => x.id == unNumQuestion);
  }

  validerQuestion(unNumQuestion:string){
    this.lesQuestions.find(x => x.id == unNumQuestion).isValide = true;
  }

}
