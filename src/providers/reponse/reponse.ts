import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Reponse } from '../../pages/models/Reponse';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ReponseProvider {

  reponses:Array<Reponse>;
  bonneReponse:number;

  constructor(public http: HttpClient) {
    this.reponses = new Array<Reponse>();
    this.bonneReponse=9;
  }
getReponse(num:string):Reponse
{
  return this.reponses.find(x => x.id == num);
}

loadDataJSONfileReponse()
  {
    var promise = new Promise(resolve=>{
      let data:Observable<any>;
      data=this.http.get('../../assets/reponse.json');
      data.subscribe(result => { 
          for(let uneReponse of result) {
            this.reponses.push(new Reponse(uneReponse));
          }
          resolve(this.reponses);
          });
        });
        return promise;
  }

  ajouterBonneReponse()
  {
    this.bonneReponse = this.bonneReponse + 1;
  }

  enleverBonneReponse()
  {
    this.bonneReponse = this.bonneReponse-1;
  }

  getBonneReponse()
  {
    return this.bonneReponse;
  }

}
