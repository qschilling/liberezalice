import { Component } from '@angular/core';
import { ViewController} from 'ionic-angular';

/**
 * Generated class for the PopoverComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'popover',
  templateUrl: 'popover.html'
})
export class PopoverComponent {

  langues: any;
  text: string;

  constructor(public ViewCtrl: ViewController) {
    this.langues = [
      {langue: "Francais"},
      {langue: "English"}
    ]
  }

  itemClick(item){
    this.ViewCtrl.dismiss(item);
  }

}
